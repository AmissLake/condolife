﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace CondoLife
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;
        private const string Server = "server_ip";
        private static readonly string ServerDefault = "192.168.15.193";

        #endregion


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }

        public static string ServerSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Server, ServerDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Server, value);
            }
        }
    }
}
