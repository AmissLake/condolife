﻿using ClassesGerais;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CondoLife.Services
{
    public class LoginService
    {
        public async Task<Usuario> FazerLogin(string login, string senha)
        {
            using (var cliente = new HttpClient())
            {
                var camposFormulario = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>("login", login),
                        new KeyValuePair<string, string>("senha", senha)
                    });
                try
                {
                    var resultado = await cliente.PostAsync("http://" + GlobalSettings.Ip + ":5000/Login/Logar", camposFormulario);

                    if (resultado.IsSuccessStatusCode)
                    {
                        var conteudoResultado = await resultado.Content.ReadAsStringAsync();
                        var resultadoLogin = JsonConvert.DeserializeObject<Usuario>(conteudoResultado);
                        return resultadoLogin;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    MessagingCenter.Send<string>(ex.StackTrace, "FalhaLogin");
                    return null;
                }
            }
        }
    }
}
