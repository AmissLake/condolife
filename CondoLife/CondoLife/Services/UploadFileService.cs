﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CondoLife.Services
{
    public class UploadFileService
    {
        public async Task<MediaFile> UploadImage()
        {
            await CrossMedia.Current.Initialize();
            if (CrossMedia.Current.IsPickPhotoSupported)
            {
                PermissionStatus status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (!status.Equals(PermissionStatus.Granted))
                {
                    var title = $"{Permission.Storage} Permission";
                    var question = $"Para adicionar uma imagem é necessario permissão para acessar o {Permission.Storage}.";
                    var positive = "Configurações";
                    var negative = "Talvez mais tarde";
                    var task = Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);

                    var result = await task;
                    if (result)
                    {
                        CrossPermissions.Current.OpenAppSettings();
                    }

                }
                if (status.Equals(PermissionStatus.Granted))
                {
                    var file = await CrossMedia.Current.PickPhotoAsync();
                    if (file != null)
                    {
                        return file;
                    }
                }
            }
            return null;
        }
    }
}
