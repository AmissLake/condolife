﻿using ClassesGerais;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CondoLife.Services
{
    public class InformationService
    {
        public async Task<bool> InserirInformacao(Informativo informativo)
        {
            using (var cliente = new HttpClient())
            {
                cliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultado = null;
                try
                {
                    var jsonContent = JsonConvert.SerializeObject(informativo);
                    var contentString = new StringContent(jsonContent.ToString(), Encoding.UTF8, "application/json");
                    resultado = await cliente.PostAsync("http://" + GlobalSettings.Ip + ":5000/Informativo/AdicionarInformativo", contentString);
                    if (resultado.IsSuccessStatusCode)
                    {
                        //MessagingCenter.Send(new Usuario(), "RegisterOk");
                        return true;
                    }
                    else
                    {
                        //MessagingCenter.Send<string>("Usuário ou senha incorretos", "FalhaLogin");
                        return false;
                    }
                }
                catch
                {
                    //MessagingCenter.Send<string>(e.Message, "FalhaLogin");
                    return false;
                }

            }
        }

        public async Task<IList<Informativo>> ListaInformativos()
        {
            using (var cliente = new HttpClient())
            {
                try
                {
                    var resultado = await cliente.GetStringAsync("http://" + GlobalSettings.Ip + ":5000/Informativo/GetTodosInformativos");
                    if (resultado.Length > 0)
                    {
                        return JsonConvert.DeserializeObject<IList<Informativo>>(resultado);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch
                {
                    return null;
                }
            }
        }

        public async Task<IList<Imagem>> ListaImagens(int Id)
        {
            using (var cliente = new HttpClient())
            {
                var camposFormulario = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>("id", Id.ToString()),
                    });

                var resultado = await cliente.PostAsync("http://" + GlobalSettings.Ip + ":5000/Informativo/GetImageList", camposFormulario);
                if (resultado.IsSuccessStatusCode)
                {
                    var resp = await resultado.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IList<Imagem>>(resp);
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
