﻿using ClassesGerais;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CondoLife.Services
{
    public class RegistrarService
    {
        public async Task<bool> Registrar(Usuario usuario)
        {
            using (var cliente = new HttpClient())
            {
                cliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultado = null;
                try
                {
                    var jsonContent = JsonConvert.SerializeObject(usuario);
                    var contentString = new StringContent(jsonContent.ToString(), Encoding.UTF8, "application/json");
                    resultado = await cliente.PostAsync("http://" + GlobalSettings.Ip + ":5000/Registro/Registrar", contentString);
                    if (resultado.IsSuccessStatusCode)
                    {
                        //MessagingCenter.Send(new Usuario(), "RegisterOk");
                        return true;
                    }
                    else
                    {
                        //MessagingCenter.Send<string>("Usuário ou senha incorretos", "FalhaLogin");
                        return false;
                    }
                }
                catch
                {
                    //MessagingCenter.Send<string>(e.Message, "FalhaLogin");
                    return false;
                }

            }
        }
    }
}
