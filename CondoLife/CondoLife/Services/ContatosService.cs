﻿using ClassesGerais;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CondoLife.Services
{
    public class ContatosService
    {
        public async Task<IList<Usuario>> ListaContatos()
        {
            using (var cliente = new HttpClient())
            {
                var resultado = await cliente.GetAsync("http://" + GlobalSettings.Ip + ":5000/Contatos/ListaContatos");
                if (resultado.IsSuccessStatusCode)
                {
                    var conteudoResultado = await resultado.Content.ReadAsStringAsync();
                    var resultadoLogin = JsonConvert.DeserializeObject<IList<Usuario>>(conteudoResultado);
                    return resultadoLogin;
                }
            }
            return null;
        }
    }
}
