﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CondoLife.Services
{
    public class CondominioService
    {
        public async Task<string> GetCondominios()
        {
            using (var cliente = new HttpClient())
            {
                return await cliente.GetStringAsync("http://192.168.15.193:5000/CondoBase/ListAll");
            }
        }
    }
}
