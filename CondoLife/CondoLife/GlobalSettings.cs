﻿using ClassesGerais;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CondoLife
{
    public static class GlobalSettings
    {
        public static Usuario Usuario { get; set; }
        public static string Ip
        {
            get
            {
                return Settings.ServerSettings;
            }
            set
            {
                Settings.ServerSettings = value;
            }
        }
    }
}
