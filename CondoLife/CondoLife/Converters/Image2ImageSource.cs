﻿using ClassesGerais;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace CondoLife.Converters
{
    class Image2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Imagem Img = value as Imagem;
            if (Img != null)
            {
                if (!String.IsNullOrEmpty(Img.ImagemBase64))
                {
                    return ImageSource.FromStream(() => new MemoryStream(System.Convert.FromBase64String(Img.ImagemBase64)));
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
