﻿using ClassesGerais;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace CondoLife.Converters
{
    public class InformationSingleImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            IList<Imagem> list = value as IList<Imagem>;
            if (list != null)
            {
                if (list.First() != null)
                {
                    return ImageSource.FromStream(() => new MemoryStream(System.Convert.FromBase64String(list.First().ImagemBase64)));
                }
                return null;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
