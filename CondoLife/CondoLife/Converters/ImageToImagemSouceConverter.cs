﻿using ClassesGerais;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace CondoLife.Converters
{
    public class ImageToImagemSouceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string ImagemBase64 = value as string;
            if (ImagemBase64 != null)
            {
                return ImageSource.FromStream(() => new MemoryStream(System.Convert.FromBase64String(ImagemBase64)));
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
