using ClassesGerais;
using CondoLife.Views.EscolhaServer;
using CondoLife.Views.Login;
using CondoLife.Views.MainNavigation;
using CondoLife.Views.Registrar;
using Newtonsoft.Json;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


[assembly: ExportFont("fontes.ttf")]
namespace CondoLife
{
    public partial class App : Application
    {
        public static string BaseImageUrl { get; } = "https://cdn.syncfusion.com/essential-ui-kit-for-xamarin.forms/common/uikitimages/";
        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider
                .RegisterLicense("MjU4Mzk3QDMxMzgyZTMxMmUzMFlkM0Q4c3ZGbllLMWpibi9JL2tSV1pveXdaWmR3endVaGxqZlI4VEFRVjg9");
            InitializeComponent();


            if (Application.Current.Properties.ContainsKey("username"))
            {
                var username = Application.Current.Properties["username"] as string;
                if (String.IsNullOrEmpty(username))
                {
                    MainPage = new NavigationPage(new LoginPage());
                }
                else
                {
                    var user = Application.Current.Properties["username"] as string;
                    GlobalSettings.Usuario = JsonConvert.DeserializeObject<Usuario>(user);
                    MainPage = new NavigationPage(new BottomNavigationPage());
                }
            }
            else
            {
                Application.Current.Properties.Add("username", "");
                MainPage = new NavigationPage(new LoginPage());
            }

        }

        protected override void OnStart()
        {
            CadastrarMensagens();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private void CadastrarMensagens()
        {
            MessagingCenter.Subscribe<Usuario>(this, "LoginOk", (usuario) =>
            {
                var ax = JsonConvert.SerializeObject(usuario);
                Application.Current.Properties["username"] = ax;
                GlobalSettings.Usuario = usuario;
                MainPage = new NavigationPage(new BottomNavigationPage());
            });

            MessagingCenter.Subscribe<object>(this, "UpdateUser", (obj) =>
            {
                var ax = JsonConvert.SerializeObject(GlobalSettings.Usuario);
                Application.Current.Properties["username"] = ax;
            });

            MessagingCenter.Subscribe<object>(this, "EscolhaServer", (msg) =>
            {
                MainPage.Navigation.PushAsync(new EscolhaServerPage());
            });

            MessagingCenter.Subscribe<object>(this, "EscolhaCondo", (msg) =>
            {
                MainPage.Navigation.PushAsync(new EscolhaCondoPage());
            });

            MessagingCenter.Subscribe<object>(this, "RealizarCadastro", (msg) =>
            {
                MainPage.Navigation.PushAsync(new SignUpPage());
            });

            MessagingCenter.Subscribe<string>(this, "TelaLogin", (msg) =>
            {
                MainPage = new NavigationPage(new LoginPage());
            });

            MessagingCenter.Subscribe<object>(this, "Logout", (msg) =>
            {
                Application.Current.Properties["username"] = null;
                GlobalSettings.Usuario = null;
                MainPage = new NavigationPage(new LoginPage());
            });
        }
    }
}
