﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.Informations
{
    /// <summary>
    /// Page to get review from customer
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddInformation
    {
        public AddInformation()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "InformativoSalvo", async (msg) =>
            {
                await DisplayAlert("Informativo", msg, "Ok");
                await Navigation.PopAsync();

            });
            MessagingCenter.Subscribe<string>(this, "FalhaSalvarInformativo", async (msg) =>
            {
                await DisplayAlert("Informativo", msg, "Ok");
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "InformativoSalvo");
            MessagingCenter.Unsubscribe<string>(this, "FalhaSalvarInformativo");

        }
    }
}