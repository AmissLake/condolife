﻿using ClassesGerais;
using CondoLife.ViewModels.InformatiosDetalhe;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.Informations
{
    /// <summary>
    /// Article with comments page.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Information
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleWithCommentsPage"/> class.
        /// </summary>
        public Information(Informativo info)
        {
            InformationViewModel informationViewModel = new InformationViewModel(info);
            InitializeComponent();
            this.BindingContext = informationViewModel;
        }
    }
}