﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.Informations.Templates
{
    /// <summary>
    /// Review template.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddInformationTemplate : Grid
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReviewTemplate"/> class.
        /// </summary>
        public AddInformationTemplate()
        {
            InitializeComponent();
        }
    }
}