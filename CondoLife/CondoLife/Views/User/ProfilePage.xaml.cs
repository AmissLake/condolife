﻿using ClassesGerais;
using CondoLife.ViewModels.User;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;


namespace CondoLife.Views.User
{
    /// <summary>
    /// Page to show chat profile page
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatProfilePage" /> class.
        /// </summary>
        public ProfilePage()
        {
            ProfileViewModel viewModel = new ProfileViewModel();
            InitializeComponent();
            this.BindingContext = viewModel;
            //this.ProfileImage.Source = App.BaseImageUrl + "ProfileImage11.png";
        }
    }
}