﻿using CondoLife.ViewModels.User;
using CondoLife.Views.EnqueteEventos;
using CondoLife.Views.Informations;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.User
{
    /// <summary>
    /// Page to show the setting.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingPage" /> class.
        /// </summary>
        public UserPage()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<object>(this, "EditProfile", (msg) =>
            {
                Navigation.PushAsync(new ProfilePage());
            });
            MessagingCenter.Subscribe<object>(this, "NovoInformativo", (msg) =>
            {
                Navigation.PushAsync(new AddInformation());
            });
            MessagingCenter.Subscribe<object>(this, "NovoEvento", (msg) =>
            {
                Navigation.PushAsync(new AddEventPage());
            });
            MessagingCenter.Subscribe<object>(this, "UserPage", (msg) =>
            {
                Navigation.PopAsync();
            });
        }
    }
}