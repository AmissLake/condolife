﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.Contatos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContatosNavigation : ContentPage
    {
        public ContatosNavigation()
        {
            InitializeComponent();
        }
    }
}