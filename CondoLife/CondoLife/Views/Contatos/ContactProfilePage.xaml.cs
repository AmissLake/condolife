﻿using ClassesGerais;
using CondoLife.ViewModels.Contatos;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.Contatos
{
    /// <summary>
    /// Page to show Contact profile page
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactProfilePage
    {
        public ContactProfilePage(Usuario user)
        {
            ContactProfileViewModel viewModel = new ContactProfileViewModel(user);
            InitializeComponent();
            this.BindingContext = viewModel;
            //this.ProfileImage.Source = App.BaseImageUrl + "ContactProfileImage.png";
        }
    }
}