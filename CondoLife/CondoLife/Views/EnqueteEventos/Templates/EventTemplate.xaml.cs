﻿using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.EnqueteEventos.Templates
{
    /// <summary>
    /// template for event detail page
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventTemplate
    {
        public EventTemplate()
        {
            InitializeComponent();
        }
    }
}