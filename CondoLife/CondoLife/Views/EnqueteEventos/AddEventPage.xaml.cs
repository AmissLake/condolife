﻿using System.Globalization;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.EnqueteEventos
{
    /// <summary>
    /// Page to get review from customer
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEventPage
    {
        public AddEventPage()
        {
            InitializeComponent();
            //this.ProductImage.Source = App.BaseImageUrl + "Image1.png";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "MensagemEvento",
                async (msg) =>
                {
                    await DisplayAlert("Add Evento", msg, "Ok");
                });
        }
    }
}