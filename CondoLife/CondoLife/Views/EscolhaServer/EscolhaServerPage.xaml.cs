﻿using CondoLife.ViewModels.EscolhaServer;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CondoLife.Views.EscolhaServer
{
    /// <summary>
    /// Page to sign in with user details.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EscolhaServerPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EscolhaServerPage" /> class.
        /// </summary>
        /// 
        public EscolhaServerViewModel viewModel { get; set; }
        public EscolhaServerPage()
        {
            InitializeComponent();
            this.viewModel = new EscolhaServerViewModel();
            this.BindingContext = this.viewModel;
        }

        protected async override void OnAppearing()
        {
            await this.viewModel.GetCondominios();
            MessagingCenter.Subscribe<string>(this, "MaximoLicensa", async (msg) =>
            {
                await DisplayAlert("Escolha Condominio", msg, "Ok");
                //MessagingCenter.Send<string>("", "TelaLogin");
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "MaximoLicensa");
        }
    }
}