﻿using ClassesGerais;
using CondoLife.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Model = ClassesGerais.Informativo;

namespace CondoLife.ViewModels.Informations
{
    /// <summary>
    /// ViewModel for article list page.
    /// </summary> 
    [Preserve(AllMembers = true)]
    public class InformationListViewModel : BaseViewModel
    {
        #region Fields

        private IList<Model> featuredStories;

        private IList<Model> latestStories;

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance for the <see cref="ArticleListViewModel" /> class.
        /// </summary>
        public InformationListViewModel()
        {
            this.MenuCommand = new Command(this.MenuClicked);
            this.BookmarkCommand = new Command(this.BookmarkButtonClicked);
            this.FeatureStoriesCommand = new Command(this.FeatureStoriesClicked);
            this.ItemSelectedCommand = new Command(this.ItemSelected);
            GetAllInformation();
            updateInformation();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the property that has been bound with the rotator view, which displays the articles featured stories items.
        /// </summary>
        public IList<Model> FeaturedStories
        {
            get
            {
                return this.featuredStories;
            }

            set
            {
                if (this.featuredStories == value)
                {
                    return;
                }

                this.featuredStories = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the property that has been bound with the list view, which displays the articles latest stories items.
        /// </summary>
        public IList<Model> LatestStories
        {
            get
            {
                return this.latestStories;
            }

            set
            {
                if (this.latestStories == value)
                {
                    return;
                }

                this.latestStories = value;
                this.NotifyPropertyChanged();
            }
        }
        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that will be executed when the menu button is clicked.
        /// </summary>
        public Command MenuCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that will be executed when the bookmark button is clicked.
        /// </summary>
        public Command BookmarkCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that will executed when the feature stories item is clicked.
        /// </summary>
        public Command FeatureStoriesCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that will be executed when an item is selected.
        /// </summary>
        public Command ItemSelectedCommand { get; set; }

        #endregion

        #region Methods

        private async void GetAllInformation()
        {
            var service = new InformationService();

            LatestStories = await service.ListaInformativos();
            if (LatestStories != null)
            {
                if (LatestStories.Count > 0)
                {
                    LatestStories = LatestStories.OrderByDescending(o => o.DataCriacao).ToList();
                    FeaturedStories = new List<Model>() { LatestStories.First() };
                }
            }

        }

        private void updateInformation()
        {
            MessagingCenter.Subscribe<object>(this, "UpdateInformationList", (obj) =>
            {
                GetAllInformation();
            });
        }

        /// <summary>
        /// Invoked when the menu button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void MenuClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when the bookmark button is clicked.
        /// </summary>
        private void BookmarkButtonClicked(object obj)
        {
            if (obj is Model article)
            {
                //         article.IsBookmarked = !article.IsBookmarked;
            }
        }

        /// <param name="obj">The object</param>
        /// <summary>
        /// Invoked when the the feature stories item is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void FeatureStoriesClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when an item is selected.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void ItemSelected(object obj)
        {
            Syncfusion.ListView.XForms.ItemTappedEventArgs item = obj as Syncfusion.ListView.XForms.ItemTappedEventArgs;
            Informativo info = item.ItemData as Informativo;
            if (info != null)
            {
                MessagingCenter.Send<Informativo>(info, "InformationSelected");
            }
        }

        #endregion
    }
}
