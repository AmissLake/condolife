﻿using ClassesGerais;
using CondoLife.Interfaces;
using CondoLife.Services;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Essentials;
using Plugin.Permissions.Abstractions;
using Plugin.Permissions;
using System.Runtime.CompilerServices;

namespace CondoLife.ViewModels.Informations
{
    /// <summary>
    /// ViewModel for review page.
    /// </summary>
    /// 

    public class ImgWrapper
    {
        public ImageSource imagem { get; set; }

        public ImgWrapper(ImageSource value)
        {
            this.imagem = value;
        }
    }

    [Preserve(AllMembers = true)]
    public class AddInformationViewModel : BaseViewModel
    {
        private ObservableCollection<ImgWrapper> imgList;
        private ImageSource imagem;
        private string texto;
        private string titulo;
        private IList<Imagem> imagens;
        #region Constructor

        public AddInformationViewModel()
        {
            this.UploadCommand = new Command<object>(this.OnUploadTapped);
            this.SubmitCommand = new Command<object>(this.OnSubmitTapped);
            imagens = new List<Imagem>();
            imgList = new ObservableCollection<ImgWrapper>();
        }

        #endregion

        #region Properties
        public ObservableCollection<ImgWrapper> ImgList
        {
            get { return this.imgList; }
            set { this.imgList = value; }
        }
        public string Texto
        {
            get { return texto; }
            set
            {
                this.texto = value;
                NotifyPropertyChanged();
            }
        }
        public ImageSource Img
        {
            get { return this.imagem; }
            set
            {
                this.imagem = value;
                imgList.Add(new ImgWrapper(value));
                NotifyPropertyChanged();
            }
        }

        public string Titulo
        {
            get { return this.titulo; }
            set
            {
                this.titulo = value;
                NotifyPropertyChanged();
            }
        }
        #endregion
        #region Command

        /// <summary>
        /// Gets or sets the value for upload command.
        /// </summary>
        public Command<object> UploadCommand { get; set; }

        /// <summary>
        /// Gets or sets the value for submit command.
        /// </summary>
        public Command<object> SubmitCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the Upload button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void OnUploadTapped(object obj)
        {
            var service = new UploadFileService();
            var imageByte = await service.UploadImage();
            if (imageByte != null)
            {
                Img = ImageSource.FromStream(() => imageByte.GetStream());
                Imagem img = new Imagem();
                byte[] fileBytes = null;
                fileBytes = new byte[imageByte.GetStream().Length];
                imageByte.GetStream().Read(fileBytes, 0, fileBytes.Length);
                img.ImagemBase64 = Convert.ToBase64String(fileBytes);
                imagens.Add(img);
            }
        }

        /// <summary>
        /// Invoked when the Submit button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void OnSubmitTapped(object obj)
        {
            var service = new InformationService();
            Informativo informativo = new Informativo { DataCriacao = DateTime.Now, Titulo = this.Titulo, Texto = this.Texto, Usuario = GlobalSettings.Usuario, imagem = imagens };
            var result = await service.InserirInformacao(informativo);
            if (result)
            {
                MessagingCenter.Send<string>("Informativo salvo com sucesso !", "InformativoSalvo");
                MessagingCenter.Send<object>(this, "UpdateInformationList");
            }
            else
            {
                MessagingCenter.Send<string>("Não foi possivel salvar, se o problema persistir entre em contato com a equipe tecnica!", "FalhaSalvarInformativo");
            }
        }

        #endregion
    }
}
