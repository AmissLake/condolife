﻿using ClassesGerais;
using CondoLife.Services;
using CondoLife.ViewModels.Informations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace CondoLife.ViewModels.EnqueteEventos
{
    /// <summary>
    /// ViewModel for review page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class AddEventPageViewModel : BaseViewModel
    {
        public ObservableCollection<ImgWrapper> imgList;
        private Imagem imagem2save;
        private ImageSource imagem;

        private string titulo;
        private string descricao;
        private DateTime date;
        private string local;

        public ObservableCollection<ImgWrapper> ImgList
        {
            get { return this.imgList; }
            set { this.imgList = value; }
        }

        public ImageSource Img
        {
            get { return this.imagem; }
            set
            {
                this.imagem = value;
                imgList.Add(new ImgWrapper(value));
                NotifyPropertyChanged();
            }
        }

        public string Titulo
        {
            get { return titulo; }
            set
            {
                this.titulo = value;
                NotifyPropertyChanged();
            }
        }

        public string Descricao
        {
            get { return this.descricao; }
            set
            {
                this.descricao = value;
                NotifyPropertyChanged();
            }
        }

        public string Local
        {
            get { return this.local; }
            set
            {
                this.local = value;
                NotifyPropertyChanged();
            }
        }

        public DateTime Date
        {
            get { return this.date; }
            set
            {
                this.date = new DateTime(value.Year, value.Month, value.Day);
                NotifyPropertyChanged();
            }
        }

        public TimeSpan Time
        {
            get { return new TimeSpan(this.date.Hour, this.date.Minute, this.date.Second); }
            set { this.date = new DateTime(this.date.Year, this.date.Month, this.date.Day, value.Hours, value.Minutes, value.Seconds); }

        }
        #region Constructor

        public AddEventPageViewModel()
        {
            date = DateTime.Now;
            imgList = new ObservableCollection<ImgWrapper>();
            this.UploadCommand = new Command<object>(this.OnUploadTapped);
            this.SubmitCommand = new Command<object>(this.OnSubmitTapped);
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the value for upload command.
        /// </summary>
        public Command<object> UploadCommand { get; set; }

        /// <summary>
        /// Gets or sets the value for submit command.
        /// </summary>
        public Command<object> SubmitCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the Upload button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void OnUploadTapped(object obj)
        {
            var service = new UploadFileService();
            var imageByte = await service.UploadImage();
            if (imageByte != null)
            {
                Img = ImageSource.FromStream(() => imageByte.GetStream());
                Imagem img = new Imagem();
                byte[] fileBytes = null;
                fileBytes = new byte[imageByte.GetStream().Length];
                imageByte.GetStream().Read(fileBytes, 0, fileBytes.Length);
                img.ImagemBase64 = Convert.ToBase64String(fileBytes);
                imagem2save = img;
            }
        }

        /// <summary>
        /// Invoked when the Submit button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void OnSubmitTapped(object obj)
        {
            var evento = new Evento()
            {
                Usuario = GlobalSettings.Usuario,
                DataHora = Date,
                NomeEvento = Titulo,
                Texto = Descricao,
                Imagem = imagem2save,
                Local = local
            };
            // Do something
            var envtoService = new EventService();
            var res = await envtoService.InserirEvento(evento);
            if (res)
            {
                MessagingCenter.Send<object>(this, "UserPage");
            }
            else
            {
                MessagingCenter.Send<string>("Falha ao salvar evento!", "MensagemEvento");
            }
        }

        #endregion
    }
}
