﻿using ClassesGerais;
using CondoLife.Models.EnqueteEventos;
using CondoLife.Services;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace CondoLife.ViewModels.EnqueteEventos
{
    /// <summary>
    /// ViewModel for event list page.
    /// </summary>
    [Preserve(AllMembers = true)]
    [DataContract]
    public class EventListViewModel : BaseViewModel
    {
        #region Fields

        private List<Evento> eventItems;

        private List<EventList> popularEventItems;

        private List<EventList> upcomingEventItems;

        public int selectedIndex;

        public string searchText;

        public string allListSearchText;

        public string upcomingListSearchText;

        public string popularListSearchText;

        #endregion

        #region Commands

        public Command SelectEvent { get; set; }

        #endregion
        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="EventListViewModel" /> class.
        /// </summary>

        public EventListViewModel()
        {
            this.SelectEvent = new Command(this.ItemSelecionado);
            this.EventItems = new List<Evento>();
            this.LoadEvents();
            // this.PopularEventItems = EventItems.Where(item => item.IsUpcoming == true).ToList();

            // this.UpcomingEventItems = EventItems.Where(item => item.IsPopular == true).ToList();

        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the event items collection.
        /// </summary>
        public List<Evento> EventItems
        {
            get
            {
                return this.eventItems;
            }

            set
            {
                this.eventItems = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the upcoming events collection.
        /// </summary>
        public List<EventList> UpcomingEventItems
        {
            get
            {
                return this.upcomingEventItems;
            }

            set
            {
                this.upcomingEventItems = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the popular events collection.
        /// </summary>
        public List<EventList> PopularEventItems
        {
            get
            {
                return this.popularEventItems;
            }

            set
            {
                this.popularEventItems = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected index of the event.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return this.selectedIndex;
            }

            set
            {
                this.selectedIndex = value;
                SearchText = string.Empty;

            }
        }

        /// <summary>
        /// Gets or sets the search text in the event.
        /// </summary>
        public string SearchText
        {
            get
            {
                return this.searchText;
            }

            set
            {
                this.searchText = value;
                UpdateSelectedText();
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the all list search text in the collection.
        /// </summary>
        public string AllListSearchText
        {
            get
            {
                return this.allListSearchText;
            }

            set
            {
                allListSearchText = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the upcoming list search text in the collection.
        /// </summary>
        public string UpcomingListSearchText
        {
            get
            {
                return this.upcomingListSearchText;
            }

            set
            {
                upcomingListSearchText = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the popular list search text collection.
        /// </summary>
        public string PopularListSearchText
        {
            get
            {
                return this.popularListSearchText;
            }

            set
            {
                popularListSearchText = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion

        #region Methods

        public async void LoadEvents()
        {
            var service = new EventService();
            var resp = await service.ListaEventos();
            if (resp != null)
            {
                this.EventItems = resp;
            }
        }

        private void UpdateSelectedText()
        {
            switch (selectedIndex)
            {
                case 0:
                    AllListSearchText = searchText;
                    break;

                case 1:
                    UpcomingListSearchText = searchText;
                    break;

                case 2:
                    PopularListSearchText = searchText;
                    break;
            }
        }

        private void ItemSelecionado(object obj)
        {
            MessagingCenter.Send<object>(this, "SelectEvent");
        }
        #endregion
    }
}
