﻿using ClassesGerais;
using CondoLife.Interfaces;
using CondoLife.Services;
using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace CondoLife.ViewModels.User
{
    /// <summary>
    /// ViewModel for profile page
    /// </summary>
    [Preserve(AllMembers = true)]
    public class ProfileViewModel : BaseViewModel
    {
        private ImageSource fotoPerfil; // = "perfil.png";
        private string nome;
        private string email;
        private string telefone;
        private string senha;
        private Endereco endereco;
        private string loginUser;
        private DateTime dataNascimento;

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="ProfileViewModel" /> class
        /// </summary>
        public ProfileViewModel()
        {
            loadPerfilImage();
            this.nome = GlobalSettings.Usuario.Name;
            this.email = GlobalSettings.Usuario.Email;
            this.telefone = GlobalSettings.Usuario.Telefone;
            this.dataNascimento = GlobalSettings.Usuario.DataNascimento;
            this.loginUser = GlobalSettings.Usuario.Login;
            this.senha = GlobalSettings.Usuario.Senha;
            this.endereco = GlobalSettings.Usuario.Endereco;
            this.EditCommand = new Command(this.EditButtonClicked);
            this.AvailableCommand = new Command(this.AvailableStatusClicked);
            this.NotificationCommand = new Command(this.NotificationOptionClicked);
            this.SaveCommand = new Command(this.SaveUser);
        }

        public DateTime DataNascimento
        {
            get { return dataNascimento; }
            set
            {
                this.dataNascimento = value;
                NotifyPropertyChanged();
            }
        }
        public string LoginUser
        {
            get { return this.loginUser; }
            set
            {
                this.loginUser = value;
                NotifyPropertyChanged();
            }
        }
        public string Senha
        {
            get { return this.senha; }
            set
            {
                this.senha = value;
                NotifyPropertyChanged();
            }
        }

        private void loadPerfilImage()
        {
            if (!String.IsNullOrEmpty(GlobalSettings.Usuario.ImagemPerfil))
            {
                fotoPerfil = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(GlobalSettings.Usuario.ImagemPerfil)));
            }
        }

        public string Estado
        {
            get { return this.endereco.Estado; }
            set
            {
                this.endereco.Estado = value;
                NotifyPropertyChanged();
            }
        }

        public string Complemento
        {
            get { return this.endereco.Complemento; }
            set
            {
                this.endereco.Complemento = value;
                NotifyPropertyChanged();
            }
        }

        public string Cidade
        {
            get { return this.endereco.Cidade; }
            set
            {
                this.endereco.Cidade = value;
                NotifyPropertyChanged();
            }
        }

        public int Numero
        {
            get { return this.endereco.Numero; }
            set
            {
                this.endereco.Numero = value;
                NotifyPropertyChanged();
            }
        }

        public string Bairro
        {
            get { return this.Endereco.Bairro; }
            set
            {
                this.endereco.Bairro = value;
                NotifyPropertyChanged();
            }
        }

        public string Rua
        {
            get { return Endereco.Logradouro; }
            set
            {
                this.endereco.Logradouro = value;
                NotifyPropertyChanged();
            }
        }

        public Endereco Endereco
        {
            get
            {
                return this.endereco;
            }
            set
            {
                this.endereco = value;
                NotifyPropertyChanged();
            }
        }

        public string Telefone
        {
            get { return this.telefone; }
            set
            {
                this.telefone = value;
                NotifyPropertyChanged();
            }
        }

        public string Nome
        {
            get { return this.nome; }
            set
            {
                this.nome = value;
                NotifyPropertyChanged();
            }
        }

        public string Email
        {
            get { return this.email; }
            set
            {
                this.email = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region Command

        public Command SaveCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the edit button is clicked.
        /// </summary>
        public Command EditCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the available status is clicked.
        /// </summary>
        public Command AvailableCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the notification option is clicked.
        /// </summary>
        public Command NotificationCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the edit button is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        /// 
        private ImageSource imageFromByte(byte[] byteArrayIn)
        {
            MemoryStream stream = new MemoryStream();
            stream.Write(byteArrayIn, 0, byteArrayIn.Length);
            return ImageSource.FromStream(() => stream);
        }

        private async void EditButtonClicked(object obj)
        {
            // Do something
            //  DependencyService.Get<ICamera>().TirarFoto();
            var service = new UploadFileService();
            var imageByte = await service.UploadImage();
            if (imageByte != null)
            {
                byte[] fileBytes = null;
                fileBytes = new byte[imageByte.GetStream().Length];
                imageByte.GetStream().Read(fileBytes, 0, fileBytes.Length);
                GlobalSettings.Usuario.ImagemPerfil = Convert.ToBase64String(fileBytes);
                bool resp = await UpdateInformation();
                if (resp)
                {
                    FotoPerfil = ImageSource.FromStream(() => imageByte.GetStream());
                    MessagingCenter.Send<object>(this, "UpdateUser");
                }
            }

        }

        private async void SaveUser()
        {
            GlobalSettings.Usuario.Endereco = Endereco;
            GlobalSettings.Usuario.Email = Email;
            GlobalSettings.Usuario.DataNascimento = DataNascimento;
            GlobalSettings.Usuario.Login = LoginUser;
            GlobalSettings.Usuario.Name = Nome;
            GlobalSettings.Usuario.Senha = Senha;
            GlobalSettings.Usuario.Telefone = Telefone;
            bool resp = await UpdateInformation();
            if (resp)
            {
                MessagingCenter.Send<object>(this, "UpdateUser");
                MessagingCenter.Send<object>(this, "UserPage");
            }
        }

        private async Task<bool> UpdateInformation()
        {
            var service = new UserService();
            var resp = await service.UpdateUser(GlobalSettings.Usuario);
            return resp;
        }

        private void RegisterMesseges()
        {
            MessagingCenter.Subscribe<byte[]>(this, "FotoTirada",
              (bytes) =>
              {
                  FotoPerfil = ImageSource.FromStream(() => new MemoryStream(bytes));
              });
        }

        public ImageSource FotoPerfil
        {
            get { return fotoPerfil; }
            private set
            {
                fotoPerfil = value;
                NotifyPropertyChanged();
            }
        }



        /// <summary>
        /// Invoked when the available status is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private async void AvailableStatusClicked(object obj)
        {
            Application.Current.Resources.TryGetValue("Gray-100", out var retVal);
            (obj as Grid).BackgroundColor = (Color)retVal;
            await Task.Delay(100);
            (obj as Grid).BackgroundColor = Color.Transparent;
        }

        /// <summary>
        /// Invoked when the notification option is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private async void NotificationOptionClicked(object obj)
        {
            Application.Current.Resources.TryGetValue("Gray-100", out var retVal);
            (obj as Grid).BackgroundColor = (Color)retVal;
            await Task.Delay(100);
            (obj as Grid).BackgroundColor = Color.Transparent;
        }

        #endregion
    }
}
