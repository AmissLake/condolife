﻿using ClassesGerais;
using CondoLife.Services;
using CondoLife.ViewModels.Login;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace CondoLife.ViewModels.Registrar
{
    /// <summary>
    /// ViewModel for sign-up page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class SignUpPageViewModel : LoginViewModel
    {
        #region Fields

        private string name;

        private string password;

        private string confirmPassword;

        private string user;


        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="SignUpPageViewModel" /> class.
        /// </summary>
        public SignUpPageViewModel()
        {
            this.LoginCommand = new Command(this.LoginClicked);
            this.SignUpCommand = new Command(this.SignUpClicked);
        }

        #endregion

        #region Property

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the name from user in the Sign Up page.
        /// </summary>
        /// 
        public string User
        {
            get
            {
                return this.user;
            }
            set
            {
                if (this.user == value)
                {
                    return;
                }
                this.user = value;
                this.NotifyPropertyChanged();
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name == value)
                {
                    return;
                }

                this.name = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the password from users in the Sign Up page.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.password == value)
                {
                    return;
                }

                this.password = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the password confirmation from users in the Sign Up page.
        /// </summary>
        public string ConfirmPassword
        {
            get
            {
                return this.confirmPassword;
            }

            set
            {
                if (this.confirmPassword == value)
                {
                    return;
                }

                this.confirmPassword = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that is executed when the Log In button is clicked.
        /// </summary>
        public Command LoginCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Sign Up button is clicked.
        /// </summary>
        public Command SignUpCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the Log in button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void LoginClicked(object obj)
        {
            MessagingCenter.Send("", "TelaLogin");
        }

        /// <summary>
        /// Invoked when the Sign Up button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void SignUpClicked(object obj)
        {
            // Do something
            if (!string.IsNullOrEmpty(this.Email) && !string.IsNullOrEmpty(this.Password) && !string.IsNullOrEmpty(this.Name) && !string.IsNullOrEmpty(this.User) && !string.IsNullOrEmpty(this.ConfirmPassword))
            {
                if (!this.Password.Equals(this.ConfirmPassword))
                {
                    MessagingCenter.Send("Senhas Precisam ser iguais", "ErroRegistrar");
                }
                else
                {
                    try
                    {
                        RegistrarService Registro = new RegistrarService();
                        Usuario usuario = new Usuario() { Login = this.User, Name = this.Name, Senha = this.Password, Email = this.Email, UsuauarioAtivo = true, Endereco = new Endereco() };
                        bool ok = await Registro.Registrar(usuario);
                        if (!ok)
                        {
                            MessagingCenter.Send("Falha ao tentar registrar", "ErroRegistrar");
                        }
                        MessagingCenter.Send("", "TelaLogin");
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.StackTrace);
                        MessagingCenter.Send("Falha ao tentar registrar", "ErroRegistrar");
                    }
                }
            }
            else
            {
                MessagingCenter.Send("Campos não podem ser vazios", "ErroRegistrar");
            }
        }

        #endregion
    }
}