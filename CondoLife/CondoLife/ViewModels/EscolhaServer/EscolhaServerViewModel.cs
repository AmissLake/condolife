﻿using ClassesGerais;
using CondoLife.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CondoLife.ViewModels.EscolhaServer
{
    public class EscolhaServerViewModel : BaseViewModel
    {
        public Command SelecionaCondo { get; set; }
        public Command SelecionarServer { get; set; }
        public Command VoltaTelaLogin { get; set; }
        private List<Condominio> condominioList;
        public List<Condominio> CondominioList
        {
            get { return condominioList; }
            set
            {
                condominioList = value;
                this.NotifyPropertyChanged(nameof(CondominioList));
            }
        }
        public EscolhaServerViewModel()
        {
            this.SelecionaCondo = new Command(this.OnClicked);
            this.SelecionarServer = new Command(this.OnSelecionaServer);
            this.VoltaTelaLogin = new Command(this.VoltaLogin);
        }



        public async Task GetCondominios()
        {
            try
            {
                CondominioService condominio = new CondominioService();
                var resultado = await condominio.GetCondominios();

                if (resultado.Length > 0)
                {
                    List<Condominio> c = new List<Condominio>();
                    var aux = JsonConvert.DeserializeObject<Condominio[]>(resultado);
                    foreach (var cond in aux)
                    {
                        c.Add(cond);
                    }
                    CondominioList = c;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace);
                MessagingCenter.Send<Exception>(e, "FalhaListagem");
            }
        }

        public void OnClicked(object obj)
        {
            Condominio condo = CondominioSelecionado;
            if (condo == null)
            {
                MessagingCenter.Send<string>("Selecione um Condominio para continuar", "MaximoLicensa");
            }
            else
            {
                if (condo.QuantidadeUsuarios + 1 <= condo.QuantidadeLicensa)
                {
                    GlobalSettings.Ip = condo.StringConexao;
                    MessagingCenter.Send<object>(this, "RealizarCadastro");
                }
                else
                {
                    MessagingCenter.Send<string>("Limite de usuarios atingido, contate o admin", "MaximoLicensa");
                }
            }
        }

        private Condominio condominioSelecionado;
        public Condominio CondominioSelecionado
        {
            get
            {
                return condominioSelecionado;
            }
            set
            {
                condominioSelecionado = value;
            }
        }

        private void OnSelecionaServer(object obj)
        {
            Condominio condo = CondominioSelecionado;
            if (condo == null)
            {
                MessagingCenter.Send<string>("Selecione um Condominio para continuar", "MaximoLicensa");
            }
            else
            {
                GlobalSettings.Ip = condo.StringConexao;
                MessagingCenter.Send<string>("", "TelaLogin");
            }
        }

        private void VoltaLogin()
        {
            MessagingCenter.Send<string>("", "TelaLogin");
        }
    }
}