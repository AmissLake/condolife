﻿using System.Runtime.Serialization;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using CondoLife.Services;
using System.Collections.Generic;
using ClassesGerais;

namespace CondoLife.ViewModels.Contatos
{
    /// <summary>
    /// ViewModel for contacts page.
    /// </summary>
    [Preserve(AllMembers = true)]
    [DataContract]
    public class ContactsViewModel : BaseViewModel
    {
        #region Fields

        private Command<object> itemTappedCommand;



        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="ContactsViewModel"/> class.
        /// </summary>
        public ContactsViewModel()
        {

        }

        #endregion

        #region Properties


        /// <summary>
        /// Gets the command that will be executed when an item is selected.
        /// </summary>
        public Command<object> ItemTappedCommand
        {
            get
            {
                return this.itemTappedCommand ?? (this.itemTappedCommand = new Command<object>(this.NavigateToNextPage));
            }
        }

        /// <summary>
        /// Gets or sets a collction of value to be displayed in contacts list page.
        /// </summary>
        [DataMember(Name = "contactsPageList")]
        //public ObservableCollection<Contact> ContactList { get; set; }
        private IList<Usuario> contactList;
        public IList<Usuario> ContactList
        {
            get { return contactList; }
            set
            {
                contactList = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when an item is selected from the contacts list.
        /// </summary>
        /// <param name="selectedItem">Selected item from the list view.</param>
        private void NavigateToNextPage(object selectedItem)
        {
            Syncfusion.ListView.XForms.ItemTappedEventArgs item = selectedItem as Syncfusion.ListView.XForms.ItemTappedEventArgs;
            Usuario user = item.ItemData as Usuario;
            MessagingCenter.Send<Usuario>(user, "ContatoSelecionado");
            // Do something
        }

        public async void ObterContatos()
        {
            var contatosService = new ContatosService();
            try
            {
                var contatos = await contatosService.ListaContatos();
                if (contatos != null)
                {
                    ContactList = contatos;
                }
            }
            catch
            {
                ContactList = null;
            }
        }
        #endregion
    }
}