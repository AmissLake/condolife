﻿using Android.App;
using Android.Content;
using Android.Runtime;
using CondoLife.Interfaces;
using Android.Provider;
using Xamarin.Forms;
using System.IO;
using CondoLife.Droid.Implementation;
using Android.Graphics;

[assembly: Xamarin.Forms.Dependency(typeof(CameraImplementation))]
namespace CondoLife.Droid.Implementation
{
    public class CameraImplementation : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, ICamera
    {
        public void TirarFoto()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            var activity = Forms.Context as Activity;
            activity.StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode,
        [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            Bitmap bitmap = (Bitmap)data.Extras.Get("data");
            MemoryStream stream = new MemoryStream();
            bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
            byte[] bytes = stream.ToArray();
            MessagingCenter.Send<byte[]>(bytes, "FotoTirada");
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}